export class Random {
	seed = 0;
	mul = 0x5deece66d;
	add = 0xb;

	constructor(seed) {
		if (seed === undefined) {
			seed = Math.floor(Math.random() * Date.now());
		}

		this.setSeed(seed);
	}
	setSeed = (n) => {
		this.seed = n ^ this.mul;
	};

	next(bits) {
		this.seed = (this.seed * this.mul + this.add) & ((1 << 48) - 1);
		return this.seed >>> (32 - bits);
	}

	nextInt(n) {
		if (!n) {
			return this.next(32);
		} else {
			return this.next(32) % n;
		}
	}
}

export const balanceArray = (arr, nextIndex) => {
	let copy = [...arr].sort((a, b) => b - a);
	let max = copy[0];

	if (arr[nextIndex] < max || copy.reverse()[0] == max) return nextIndex;

	let l = nextIndex;
	let r = nextIndex;
	nextIndex = -1;

	while (nextIndex < 0) {
		if (l > 0) l--;
		if (r < arr.length) r++;

		nextIndex = arr[l] < max ? l : arr[r] < max ? r : -1;
	}

	return nextIndex;
};

export const logGen = (s, isString = true) => {
	let input = isString ? `\"${s}\"` : `${s}`;
	return `console.log(${input});`;
};

export const logArray = (n, groupSize = 1) => {
	return [...Array(n)].map((e, i) =>
		logGen("" + Math.ceil((i + 1) / groupSize))
	);
};

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

export const characterArray = (amount) => {
	return alphabet.slice(0, amount).split("");
};

export const randomizeArray = (arr, rand, position = undefined) => {
	let length = arr.length;
	position = position ? position : rand.nextInt(length);
	let result = [...Array(length)];
	result[position] = arr[0];
	arr = arr.slice(1);

	for (let i = 0; i < length; i++) {
		if (i == position) continue;

		let index = rand.nextInt(arr.length);
		result[i] = arr[index];
		arr = arr.filter((e, k) => k != index);
	}

	return result;
};
