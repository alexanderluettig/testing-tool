const isProd = process.env.NODE_ENV === "production";

/** @type {import('next').NextConfig} */
const nextConfig = {
	basePath: isProd ? "/testing-tool" : "",
	reactStrictMode: true,
	assetPrefix: isProd ? "/testing-tool/" : "",
	images: {
		loader: "akamai",
		path: "",
	},
};

module.exports = nextConfig;
