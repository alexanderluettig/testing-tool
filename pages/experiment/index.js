import { useState, useEffect } from "react";
import Link from "next/link";
import {
	experimentNames,
	experimentQuestions,
} from "../../data/experimentData";
import generateExamples from "../../data/gen/Examples";
import generateLoopCount from "../../data/gen/LoopCount";
import generateSwitchIdentification from "../../data/gen/SwitchIdent";
import generateSwitchOutput from "../../data/gen/SwitchOutput";
import download from "../../utils/download";
import generateLoopIdentification from "../../data/gen/LoopIdent";
import styles from "../../styles/Experiments.module.css";

export default function Experiment() {
	const [current, setCurrent] = useState(1);
	const [currentTask, setCurrentTask] = useState({
		nr: 0,
		snippet: "",
		solution: 0,
		type: "",
	});
	const [done, setDone] = useState(false);
	const [lastTime, setLastTime] = useState(Date.now());
	const [loading, setLoading] = useState(false);
	const [name, setName] = useState("");
	const [result, setResult] = useState([]);
	const [pause, setPause] = useState(false);
	const [seed, setSeed] = useState(0);
	const [submited, setSubmit] = useState(false);
	const [tasks, setTasks] = useState([]);
	const [type, setType] = useState(experimentNames[0]);
	const [fontSize, setFontSize] = useState(14);

	const endPause = () => {
		setPause(false);
		setLastTime(Date.now());
	};

	const changeFontSize = (event) => {
		setFontSize(event.target.value);
	};

	const reset = () => {
		setDone(false);
		setSubmit(false);
		setResult([]);
		setPause(false);
		setTasks([]);
		setCurrent(1);
		setCurrentTask({
			nr: 0,
			snippet: "",
			solution: 0,
			type: "",
		});
	};

	const submitSettings = async () => {
		setSubmit(true);
		setLoading(true);

		var generatedTasks = [];

		switch (type) {
			case "Switch-Identification":
				generatedTasks = generateSwitchIdentification(seed);
				break;
			case "Switch-Output":
				generatedTasks = generateSwitchOutput(seed);
				break;
			case "Loop-Count":
				generatedTasks = generateLoopCount(seed);
				break;
			case "Loop-Identification":
				generatedTasks = generateLoopIdentification(seed);
				break;
			case "Examples":
				generatedTasks = generateExamples();
				break;
			default:
				break;
		}

		setTasks(generatedTasks);
		setCurrentTask(generatedTasks[0]);

		setLoading(false);
		setPause(true);
	};

	const nextTask = async (answer) => {
		var time = Date.now();
		var newResult = [
			...result,
			{
				nr: current,
				answer: type.endsWith("-Identification")
					? answer == 1
					: answer == ""
					? 0
					: answer,
				solution: currentTask.solution,
				type: currentTask.type,
				time: time - lastTime,
			},
		];

		setResult(newResult);

		if (!done) {
			setPause(true);
			setCurrent(current + 1);
			setCurrentTask(tasks[current]);
		}

		if (current === tasks.length) {
			setDone(true);
			setPause(false);
		}
	};

	const initDownload = () => {
		var header = "nr;type;solution;answer;time\n";
		var data = result
			.map((e) => `${e.nr};${e.type};${e.solution};${e.answer};${e.time}`)
			.join("\n");

		download(`${name}_${type}.csv`, header + data);
	};

	return (
		<div className="App">
			{loading ? (
				<div>Loading ... </div>
			) : !submited ? (
				<div className={styles.settings}>
					<Link passHref href={"/"}>
						<div
							style={{
								textAlign: "center",
								padding: "2px",
								height: "30px",
								width: "140px",
								fontSize: "20px",
								border: "none",
								backgroundImage: "linear-gradient(gray,white)",
							}}
						>
							Startseite
						</div>
					</Link>
					<div>
						<label>Name:</label>
						<input
							onChange={(event) => setName(event.target.value)}
							value={name}
						/>
					</div>
					<div>
						<label>Seed:</label>
						<input
							onChange={(event) => setSeed(event.target.value)}
							value={seed}
						/>
					</div>
					<div>
						<select value={type} onChange={(e) => setType(e.target.value)}>
							{experimentNames.map((e, i) => (
								<option key={i}>{e}</option>
							))}
						</select>
					</div>
					<button disabled={name == ""} onClick={submitSettings}>
						Submit
					</button>
				</div>
			) : pause ? (
				<Pause
					endPause={endPause}
					question={experimentQuestions[type]}
					type={currentTask.type}
					maxAmount={tasks.length}
					current={current}
				></Pause>
			) : !done ? (
				<Tasks
					task={currentTask}
					question={experimentQuestions[type]}
					current={current}
					maxAmount={tasks.length}
					callBack={nextTask}
					fontSize={fontSize}
					changeFontSize={changeFontSize}
				></Tasks>
			) : done ? (
				<div className={styles.end}>
					<Link passHref href={"/"}>
						<div
							style={{
								textAlign: "center",
								padding: "2px",
								height: "30px",
								margin: "10px",
								width: "140px",
								fontSize: "20px",
								border: "none",
								backgroundImage: "linear-gradient(gray,white)",
							}}
						>
							Startseite
						</div>
					</Link>
					<button
						style={{
							textAlign: "center",
							padding: "2px",
							height: "30px",
							width: "140px",
							margin: "10px",
							fontSize: "20px",
							border: "none",
							backgroundImage: "linear-gradient(gray,white)",
						}}
						onClick={() => reset()}
					>
						Experimente
					</button>
					<div style={{ margin: "10px" }}>Danke fürs Teilnehmen :)</div>
					<button
						style={{
							width: "300px",
							height: "50px",
							fontSize: "30px",
							fontWeight: "600",
						}}
						onClick={() => initDownload()}
					>
						Download Result
					</button>
				</div>
			) : (
				<div>Error</div>
			)}
		</div>
	);
}

const Tasks = ({
	question,
	task,
	current,
	maxAmount,
	callBack,
	fontSize,
	changeFontSize,
}) => {
	const [answer, setAnswer] = useState("");
	question = question.replace("TYPE", task.type);

	return (
		<div>
			<div className={styles.task}>
				<p>Aufgabe {`${current} / ${maxAmount}`}</p>
				<p>Frage: {question}</p>
				<div style={{ paddingTop: "0px", padding: "5px" }}>
					<label>Font Size: </label>
					<input
						type={"range"}
						onChange={(event) => changeFontSize(event)}
						value={fontSize}
						min={10}
						max={20}
					/>
				</div>
				<textarea
					className={styles.code}
					readOnly={true}
					value={task.snippet}
					style={{ fontSize: +fontSize }}
				></textarea>
				<div>
					<label>Antwort: </label>
					<input
						type="number"
						autoFocus
						value={answer}
						onChange={(event) => setAnswer(event.target.value)}
						onKeyDown={(e) => {
							if (e.code === "Enter") callBack(answer);
						}}
					></input>
				</div>
				<div>
					<button onClick={callBack}>Next Tasks</button>
				</div>
			</div>
		</div>
	);
};

const Pause = ({ endPause, question, type, maxAmount, current }) => {
	question = question.replace("TYPE", type);

	useEffect(() => {
		const handleClick = () => {
			document.getElementById("Button").focus();
		};
		window.addEventListener("click", handleClick);

		return () => {
			window.removeEventListener("click", handleClick);
		};
	});

	return (
		<div className={styles.pause}>
			<div>Pause :)</div>
			<div>{`Aufgabe ${current}/${maxAmount}`}</div>
			<div>Nächste Frage: {question}</div>
			<div>Enter um weiter zumachen</div>
			<button
				id={"Button"}
				style={{
					hidden: true,
					outline: 0,
					border: "0px",
					backgroundColor: "white",
				}}
				autoFocus
				onKeyDown={(e) => {
					if (e.code === "Enter") endPause();
				}}
			></button>
		</div>
	);
};
