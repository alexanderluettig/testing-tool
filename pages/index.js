import Link from "next/link";
import styles from "../styles/Home.module.css";

export default function Home() {
	return (
		<div>
			<div className={styles.LinkContainer}>
				<Link passHref href={"/experiment"}>
					<div className={styles.Link}>Experimente</div>
				</Link>
				<Link passHref href={"/examples"}>
					<div className={styles.Link}>Beispiele</div>
				</Link>
			</div>
			<div className={styles.footer}>Olli stinkt</div>
		</div>
	);
}
