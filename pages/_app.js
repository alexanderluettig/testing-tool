import "../styles/globals.css";

function MyApp({ Component, pageProps }) {
	return (
		<>
			<title>Testing Tool BA</title>
			<Component {...pageProps} />
		</>
	);
}

export default MyApp;
