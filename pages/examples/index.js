import Link from "next/link";
import examples from "../../data/examples";
import styles from "../../styles/Examples.module.css";

export default function Examples() {
	return (
		<div className={styles.container}>
			<div>
				<div
					style={{
						display: "flex",
						alignItems: "center",
						justifyContent: "center",
					}}
				>
					<Link passHref href={"/"}>
						<div
							style={{
								textAlign: "center",
								padding: "2px",
								height: "30px",
								width: "140px",
								fontSize: "20px",
								border: "none",
								backgroundImage: "linear-gradient(gray,white)",
							}}
						>
							Startseite
						</div>
					</Link>
				</div>
				<details>
					<summary>Switches</summary>
					<div className={styles.category}>
						<details>
							<summary>Reference Syntax</summary>
							<textarea
								disabled
								className={styles.code}
								style={{ height: "70vh" }}
								defaultValue={
									examples.referenceSyntax.switch +
									"\n\n\n" +
									examples.referenceSyntax.if +
									"\n\n\n" +
									examples.referenceSyntax.lambdaSwitch
								}
							/>
						</details>
						<details>
							<summary>Beispiel Fragestellung 1</summary>
							<div>
								!!!Hinweis: lamba-switch = SWITCH... und switch = switch...
							</div>
							<ExampleTask
								content={examples.tasks["switch-ident"]}
							></ExampleTask>
						</details>
						<details>
							<summary>Beispiel Fragestellung 2</summary>
							<ExampleTask
								content={examples.tasks["switch-output"]}
							></ExampleTask>
						</details>
					</div>
				</details>
			</div>
			<div>
				<details>
					<summary>Loops</summary>
					<div className={styles.category}>
						<details>
							<summary>Reference Syntax</summary>
							<textarea
								disabled
								className={styles.code}
								defaultValue={
									examples.referenceSyntax.for +
									"\n\n\n" +
									examples.referenceSyntax.forIterator +
									"\n\n\n" +
									examples.referenceSyntax.forEach +
									"\n\n\n" +
									examples.referenceSyntax.repeat
								}
							/>
						</details>
						<details>
							<summary>Beispiel Fragestellung 1</summary>
							<ExampleTask
								content={examples.tasks["loops-ident"]}
							></ExampleTask>
						</details>
						<details>
							<summary>Beispiel Fragestellung 2</summary>
							<ExampleTask
								content={examples.tasks["loops-count"]}
							></ExampleTask>
						</details>
					</div>
				</details>
			</div>
		</div>
	);
}

const ExampleTask = ({ content }) => {
	return (
		<div className={styles.taskContainer}>
			<div>Frage: {content.question}</div>
			<div>Mögliche Antworten: {content.possibleAnswers}</div>
			<textarea
				disabled
				className={styles.code}
				defaultValue={content.snippet}
			/>
			<div>Antwort: {content.answer}</div>
		</div>
	);
};
