export default function generateExamples() {
	var result = [
		{
			nr: 1,
			solution: 1,
			snippet:
				'list.add(498)\n\nSWITCH(Element)\n\t.CASE(901,\n\t\t() => log("718")\n\t)\n\t.CASE(901,\n\t\t() => log("718")\n\t)\n\t.DEFAULT(\n\t\t() => log("default")\n\t);\n\nawait input("Please input a number: ");\n\nswitch(c){\n\tcase 718:\n\t\tlog("430");\n\t\tbreak;\n\tdefault:\n\t\tlog("default");\n\t\tbreak;\n}',
			type: "lambda-switch",
		},
		{
			nr: 2,
			solution: 2,
			snippet:
				'SWITCH("E")\n\t.CASE("G", \n\t\t() => log("1"))\n\t.CASE("F", \n\t\t() => log("1"))\n\t.CASE("K", \n\t\t() => log("1"))\n\t.CASE("H", \n\t\t() => log("1"))\n\t.CASE("J", \n\t\t() => log("2"))\n\t.CASE("A", \n\t\t() => log("2"))\n\t.CASE("L", \n\t\t() => log("2"))\n\t.CASE("E", \n\t\t() => log("2"))\n\t.CASE("B", \n\t\t() => log("3"))\n\t.CASE("I", \n\t\t() => log("3"))\n\t.CASE("C", \n\t\t() => log("3"))\n\t.DEFAULT(\n\t\t() => log("3"));',
			type: "lambda-switch",
		},
		{
			nr: 3,
			solution: 0,
			snippet:
				"for(let c : list){ \n\tlog(c); \n }\n\nrepeat(collection,\n\t(c) => log(c)\n);)\n\nlist.add(462)\n\nlist = list.map(e => e != 441);",
			type: "forEach",
		},
		{
			nr: 4,
			snippet:
				'for(let i = 0; i < 33; i++){\n\trepeat(collection,\n\t\t(Element) => log(Element)\n\t);)\n}\n\nrepeat(collection,\n\t(c) => log(c)\n);)\n\nSWITCH(Element)\n\t.CASE(873,\n\t() => {\n\t\trepeat(list,\n\t\t\t(Element) => log(Element)\n\t\t);)})\n\t.DEFAULT(\n\t\t() => log("default")\n\t);\n\nlist.add(353)\n\nlist = list.map(e => e != 1140);\n\nfor(let i = 0; i < list; i++){ \n\tlog(list[i]); \n}',
			solution: "3",
			type: "repeat",
		},
	];

	return result;
}
