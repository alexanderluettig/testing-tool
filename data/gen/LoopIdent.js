import { Random, randomizeArray } from "../../utils/Tools";

//Experiment Options
const amountTasks = 240;
const lineBreaks = true;
const randomIndentation = false;
const emptyLines = true;

//Variables
const amountTypes = 4;
const amountSeen = 2;
const snippetLength = 4;
const possibleElementNames = ["Element", "Current", "e", "c"];
const possibleListNames = ["list", "elements", "collection"];

//Executed Method
const generateLoopIdentification = (seed) => {
	let random = new Random(seed);

	//Generation/Balancing
	let tasks = [];
	for (let i = 0; i < amountTasks / (amountSeen * amountTypes); i++) {
		for (let k = 0; k < amountTypes; k++) {
			for (let j = 0; j < amountSeen; j++) {
				tasks.push({
					type:
						k == 0
							? "for"
							: k == 1
							? "for-iterator"
							: k == 2
							? "forEach"
							: "repeat",
					seen: j == 0,
				});
			}
		}
	}
	tasks = randomizeArray(tasks, random);

	return tasks.map((e, i) => {
		return {
			nr: i,
			solution: e.seen,
			type: e.type,
			snippet: generation(e, random),
		};
	});
};

// Snippet Generation
const generation = (task, rand) => {
	let snippetParts = [];

	if (task.seen) {
		let element = "";
		switch (task.type) {
			case "for":
				element = genFor(rand);
				break;
			case "for-iterator":
				element = genForIterator(rand);
				break;
			case "forEach":
				element = genForEach(rand);
				break;
			case "repeat":
				element = genRepeat(rand);
				break;
			default:
				element = "ERROR";
				break;
		}

		let hideElement = rand.nextInt(2) == 0;
		if (hideElement) {
			element = genNoise(rand, task, element);
		}

		snippetParts.push(element);
	}

	while (snippetParts.length < snippetLength) {
		snippetParts.push(genNoise(rand, task));
	}

	if (randomIndentation) {
		snippetParts = snippetParts.map((p) =>
			p.replace(/\n/g, "\n" + "\t".repeat(rand.nextInt(2)))
		);
	}

	snippetParts = randomizeArray(snippetParts, rand);
	return snippetParts.join("\n".repeat(1 + +emptyLines));
};

const genNoise = (rand, task, elementToHide = "default") => {
	let part = "";

	if (elementToHide != "default") {
		elementToHide = elementToHide.replace(/\n/g, "\n\t");
		part = hideElement(elementToHide, rand, task);
	} else {
		while (part == "") {
			let number = rand.nextInt(9);

			if (number == 0) {
				part = `let ${
					possibleElementNames[rand.nextInt(possibleElementNames.length)]
				} = ${rand.nextInt(1337)};`;
			} else if (number == 1 && !(task.type == "for" && !task.seen)) {
				part = genFor(rand);
			} else if (number == 2 && !(task.type == "for-iterator" && !task.seen)) {
				part = genForIterator(rand);
			} else if (number == 3 && !(task.type == "forEach" && !task.seen)) {
				part = genForEach(rand);
			} else if (number == 4 && !(task.type == "repeat" && !task.seen)) {
				part = genRepeat(rand);
			} else if (number == 5) {
				part = `await input("Please input a number: ");`;
			} else if (number == 6) {
				part = `list = list.map(e => e != ${rand.nextInt(1337)});`;
			} else if (number == 7 && task.type != "forEach") {
				part = `list.forEach(e =>\n\tlog(e)\n);`;
			} else if (number == 8) {
				part = `list.add(${rand.nextInt(1337)})`;
			}
		}
	}
	return part;
};

const hideElement = (elementToHide, rand, task) => {
	var part = "";
	while (part == "") {
		let number = rand.nextInt(5);
		if (number == 0 && !(task.type == "for" && !task.seen)) {
			part = `for(let i = 0; i < ${rand.nextInt(
				1337
			)}; i++){\n\t${elementToHide}\n}`;
		} else if (number == 1) {
			part = `if(element == 0){\n\t${elementToHide}\n}\nelse{\n\tlog(${rand.nextInt(
				1337
			)});\n}`;
		} else if (number == 2) {
			part = `if(element == 0){\n\tlog(${rand.nextInt(
				1337
			)});\n}\nelse{\n\t${elementToHide}\n}`;
			break;
		} else if (number == 3) {
			elementToHide = elementToHide.replace(/\n/g, "\n\t");
			part = `switch(${
				possibleElementNames[rand.nextInt(possibleElementNames.length)]
			}){\n\tcase ${rand.nextInt(
				1337
			)}:\n\t\t${elementToHide}\n\t\tbreak;\n${`\tcase ${rand.nextInt(
				1337
			)}: ${"\n\t\t".repeat(+lineBreaks)}log("${rand.nextInt(
				1337
			)}"); ${"\n\t\t".repeat(+lineBreaks)}break;\n`.repeat(
				0
			)}\tdefault: ${"\n\t\t".repeat(
				+lineBreaks
			)}log("default"); ${"\n\t\t".repeat(+lineBreaks)}break;\n}`;
		} else if (number == 4) {
			elementToHide = elementToHide.replace(/\n/g, "\n\t");
			part = `SWITCH(${
				possibleElementNames[rand.nextInt(possibleElementNames.length)]
			})\n\t.CASE(${rand.nextInt(
				1337
			)},\n\t() => {\n\t\t${elementToHide}})\n${`\t.CASE(${rand.nextInt(
				1337
			)},${"\n\t\t".repeat(+lineBreaks)}() => log("${rand.nextInt(
				1337
			)}")${"\n\t".repeat(+lineBreaks)})\n`.repeat(
				0
			)}\t.DEFAULT(${"\n\t\t".repeat(
				+lineBreaks
			)}() => log("default")${"\n\t".repeat(+lineBreaks)});`;
		}
	}

	return part;
};

const genFor = (rand) => {
	let listName = possibleListNames[rand.nextInt(possibleListNames.length)];
	return `for(let i = 0; i < ${listName}; i++){ ${"\n\t".repeat(
		+lineBreaks
	)}log(${listName}[i]); ${"\n".repeat(+lineBreaks)}}`;
};

const genForIterator = (rand) => {
	let elementName =
		possibleElementNames[rand.nextInt(possibleElementNames.length)];

	return `for(let ${elementName} : ${
		possibleListNames[rand.nextInt(possibleListNames.length)]
	}){ ${"\n\t".repeat(+lineBreaks)}log(${elementName}); ${"\n".repeat(
		+lineBreaks
	)} }`;
};

const genForEach = (rand) => {
	let elementName =
		possibleElementNames[rand.nextInt(possibleElementNames.length)];
	return `${
		possibleListNames[rand.nextInt(possibleListNames.length)]
	}.forEach(${"\n\t".repeat(
		+lineBreaks
	)}(${elementName}) => log(${elementName})${"\n".repeat(+lineBreaks)});`;
};

const genRepeat = (rand) => {
	let elementName =
		possibleElementNames[rand.nextInt(possibleElementNames.length)];
	return `repeat(${
		possibleListNames[rand.nextInt(possibleListNames.length)]
	},${"\n\t".repeat(
		+lineBreaks
	)}(${elementName}) => log(${elementName})${"\n".repeat(+lineBreaks)});)`;
};

export default generateLoopIdentification;
