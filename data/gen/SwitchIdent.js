import { Random, randomizeArray } from "../../utils/Tools";

//Experiment Options
const amountTasks = 180;
const lineBreaks = true;
const randomIndentation = false;
const emptyLines = true;

//Variables
const amountTypes = 3;
const amountSeen = 2;
const snippetLength = 4;
const possibleElementNames = ["Element", "Current", "e", "c"];
const condLength = 2;

//Executed Method
const generateSwitchIdentification = (seed) => {
	let random = new Random(seed);

	//Generation/Balancing
	let tasks = [];
	for (let i = 0; i < amountTasks / (amountSeen * amountTypes); i++) {
		for (let k = 0; k < amountTypes; k++) {
			for (let j = 0; j < amountSeen; j++) {
				tasks.push({
					type: k == 0 ? "switch" : k == 1 ? "switch-lambda" : "if/else",
					seen: j == 0,
				});
			}
		}
	}
	tasks = randomizeArray(tasks, random);
	return tasks.map((e, i) => {
		return {
			nr: i,
			solution: e.seen,
			type: e.type,
			snippet: generation(e, random),
		};
	});
};

// Snippet Generation
const generation = (task, rand) => {
	let snippetParts = [];

	if (task.seen) {
		let element = "";
		switch (task.type) {
			case "switch":
				element = genSwitch(rand);
				break;
			case "switch-lambda":
				element = genLambdaSwitch(rand);
				break;
			case "if/else":
				element = genIf(rand);
				break;
			default:
				element = "ERROR";
				break;
		}

		let hideElement = rand.nextInt(2) == 0;
		if (hideElement) {
			element = genNoise(rand, task, element);
		}

		snippetParts.push(element);
	}

	while (snippetParts.length < snippetLength) {
		snippetParts.push(genNoise(rand, task));
	}

	if (randomIndentation) {
		snippetParts = snippetParts.map((p) =>
			p.replace(/\n/g, "\n" + "\t".repeat(rand.nextInt(2)))
		);
	}

	snippetParts = randomizeArray(snippetParts, rand);
	return snippetParts.join("\n".repeat(1 + +emptyLines));
};

const genNoise = (rand, task, elementToHide = "default") => {
	let part = "";

	if (elementToHide != "default") {
		elementToHide = elementToHide.replace(/\n/g, "\n\t");
		part = hideElement(elementToHide, rand, task);
	} else {
		while (part == "") {
			let number = rand.nextInt(8);

			if (number == 0) {
				part = `let ${
					possibleElementNames[rand.nextInt(possibleElementNames.length)]
				} = ${rand.nextInt(1337)};`;
			} else if (number == 1 && !(task.type == "switch" && !task.seen)) {
				part = genSwitch(rand);
			} else if (number == 2 && !(task.type == "switch-lambda" && !task.seen)) {
				part = genLambdaSwitch(rand);
			} else if (number == 3 && !(task.type == "if/else" && !task.seen)) {
				part = genIf(rand);
			} else if (number == 4) {
				part = `await input("Please input a number: ");`;
			} else if (number == 5) {
				part = `list = list.map(e => e != ${rand.nextInt(1337)});`;
			} else if (number == 6) {
				part = `list.forEach(e =>\n\tlog(e)\n);`;
			} else if (number == 7) {
				part = `list.add(${rand.nextInt(1337)})`;
			}
		}
	}
	return part;
};

const hideElement = (elementToHide, rand, task) => {
	var part = "";
	while (part == "") {
		let number = rand.nextInt(5);
		if (number == 0) {
			part = `for(let i = 0; i < ${rand.nextInt(
				1337
			)}; i++){\n\t${elementToHide}\n}`;
		} else if (number == 1 && !(task.type == "if/else" && !task.seen)) {
			part = `if(element == 0){\n\t${elementToHide}\n}\nelse{\n\tlog(${rand.nextInt(
				1337
			)});\n}`;
		} else if (number == 2 && !(task.type == "if/else" && !task.seen)) {
			part = `if(element == 0){\n\tlog(${rand.nextInt(
				1337
			)});\n}\nelse{\n\t${elementToHide}\n}`;
			break;
		} else if (number == 3 && !(task.type == "switch" && !task.seen)) {
			elementToHide = elementToHide.replace(/\n/g, "\n\t");
			part = `switch(${
				possibleElementNames[rand.nextInt(possibleElementNames.length)]
			}){\n\tcase ${rand.nextInt(
				1337
			)}:\n\t\t${elementToHide}\n\t\tbreak;\n${`\tcase ${rand.nextInt(
				1337
			)}: ${"\n\t\t".repeat(+lineBreaks)}log("${rand.nextInt(
				1337
			)}"); ${"\n\t\t".repeat(+lineBreaks)}break;\n`.repeat(
				0
			)}\tdefault: ${"\n\t\t".repeat(
				+lineBreaks
			)}log("default"); ${"\n\t\t".repeat(+lineBreaks)}break;\n}`;
		} else if (number == 4 && !(task.type == "switch-lambda" && !task.seen)) {
			elementToHide = elementToHide.replace(/\n/g, "\n\t");
			part = `SWITCH(${
				possibleElementNames[rand.nextInt(possibleElementNames.length)]
			})\n\t.CASE(${rand.nextInt(
				1337
			)},\n\t() => {\n\t\t${elementToHide}})\n${`\t.CASE(${rand.nextInt(
				1337
			)},${"\n\t\t".repeat(+lineBreaks)}() => log("${rand.nextInt(
				1337
			)}")${"\n\t".repeat(+lineBreaks)})\n`.repeat(
				0
			)}\t.DEFAULT(${"\n\t\t".repeat(
				+lineBreaks
			)}() => log("default")${"\n\t".repeat(+lineBreaks)});`;
		}
	}

	return part;
};

const genSwitch = (rand) => {
	let length = rand.nextInt(condLength) + 2;

	let part = `switch(${
		possibleElementNames[rand.nextInt(possibleElementNames.length)]
	}){\n${`\tcase ${rand.nextInt(1337)}: ${"\n\t\t".repeat(
		+lineBreaks
	)}log("${rand.nextInt(1337)}"); ${"\n\t\t".repeat(
		+lineBreaks
	)}break;\n`.repeat(length - 1)}\tdefault: ${"\n\t\t".repeat(
		+lineBreaks
	)}log("default"); ${"\n\t\t".repeat(+lineBreaks)}break;\n}`;

	return part;
};
const genLambdaSwitch = (rand) => {
	let length = rand.nextInt(condLength) + 2;
	let part = `SWITCH(${
		possibleElementNames[rand.nextInt(possibleElementNames.length)]
	})\n${`\t.CASE(${rand.nextInt(1337)},${"\n\t\t".repeat(
		+lineBreaks
	)}() => log("${rand.nextInt(1337)}")${"\n\t".repeat(+lineBreaks)})\n`.repeat(
		length - 1
	)}\t.DEFAULT(${"\n\t\t".repeat(
		+lineBreaks
	)}() => log("default")${"\n\t".repeat(+lineBreaks)});`;
	return part;
};
const genIf = (rand) => {
	let length = rand.nextInt(condLength) + 2;
	let part = "";

	if (lineBreaks) {
		part = `if(${
			possibleElementNames[rand.nextInt(possibleElementNames.length)]
		} == ${rand.nextInt(1337)}){\n\tlog(${rand.nextInt(
			1337
		)});\n}\n${`else if(${
			possibleElementNames[rand.nextInt(possibleElementNames.length)]
		} == ${rand.nextInt(1337)}){\n\tlog(${rand.nextInt(1337)});\n}\n`.repeat(
			length - 2
		)}else{\n\tlog(${rand.nextInt(1337)});\n}`;
	} else {
		part = `if(${
			possibleElementNames[rand.nextInt(possibleElementNames.length)]
		} == ${rand.nextInt(1337)}) log(${rand.nextInt(1337)});\n${`else if(${
			possibleElementNames[rand.nextInt(possibleElementNames.length)]
		} == ${rand.nextInt(1337)}) log(${rand.nextInt(1337)});\n`.repeat(
			length - 2
		)}else log(${rand.nextInt(1337)});`;
	}

	return part;
};

export default generateSwitchIdentification;
