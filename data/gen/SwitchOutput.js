import {
	characterArray,
	logArray,
	Random,
	randomizeArray,
} from "../../utils/Tools";

//Experiment Options
const amountTasks = 144;
const lineBreaks = true;

//Variables
const groupSize = 4;
const snippetLength = 12;
const amountTypes = 3;

const generateSwitchOutput = (seed) => {
	let random = new Random(seed);

	//Generation/Balancing
	let tasks = [];
	for (let i = 0; i < amountTasks / (snippetLength * amountTypes); i++) {
		for (let k = 0; k < amountTypes; k++) {
			for (let j = 1; j <= snippetLength; j++) {
				tasks.push({
					type: k == 0 ? "switch" : k == 1 ? "switch-lambda" : "if/else",
					place: j,
					group: Math.ceil(j / groupSize),
				});
			}
		}
	}
	tasks = randomizeArray(tasks, random);
	return tasks.map((e, i) => {
		return {
			nr: i,
			solution: e.group,
			type: e.type,
			snippet: generation(e, random),
		};
	});
};

const generation = (task, rand) => {
	switch (task.type) {
		case "switch":
			return genNormal(task.place, snippetLength, rand, lineBreaks);
		case "switch-lambda":
			return genLambda(task.place, snippetLength, rand, lineBreaks);
		case "if/else":
			return genIf(task.place, snippetLength, rand);
		default:
			return "ERROR";
	}
};

const normalCaseGen = (_case, logs, lineBreaks) => {
	let result = `\tcase ${_case}:${lineBreaks ? "\n" : ""}`;
	for (let e of logs) {
		result += `${lineBreaks ? "\t" : ""}\t${e}\n`;
	}

	return result + `${!lineBreaks ? "\t" : ""}\t\tbreak;\n`;
};

const normalDefaultGen = (logs, lineBreaks) => {
	let result = `\tdefault:${lineBreaks ? "\n" : ""}`;

	for (let e of logs) {
		result += `${lineBreaks ? "\t" : ""}\t${e}\n`;
	}

	return result + `${!lineBreaks ? "\t" : ""}\t\tbreak;\n`;
};

const normalSwitchGen = (startingValue, cases, defaultCase) => {
	let result = `switch (${startingValue}) {\n`;

	for (let e of cases) {
		result += e;
	}

	return result + defaultCase + "}";
};

const ifGen = (value, checkValue, log) => {
	return `if(${value} == ${checkValue}){\n\t${log}\n}\n`;
};

const elseIfGen = (value, checkValue, log) => {
	return `else if(${value} == ${checkValue}){\n\t${log}\n}\n`;
};

const elseGen = (log) => {
	return `else{\n\t${log}\n}\n`;
};

const lambdaCaseGen = (value, log, lineBreaks) => {
	return `\t.CASE(${value}, ${lineBreaks ? "\n\t\t" : ""}() => ${log.slice(
		0,
		-1
	)})\n`;
};

const lambdaDefaultGen = (log, lineBreaks) => {
	return `\t.DEFAULT(${lineBreaks ? "\n\t\t" : ""}() => ${log.slice(
		0,
		-1
	)});\n`;
};

const lambdaSwitchGen = (startingValue, cases, defaultCase) => {
	let result = `SWITCH(${startingValue})\n`;

	for (let e of cases) {
		result += e;
	}

	return result + defaultCase;
};

export const genNormal = (read, maxAmount, rand, lineBreaks) => {
	let result = "";

	let logs = logArray(maxAmount, groupSize);
	let cases = [];
	let defaultCase = normalDefaultGen(logs.slice(-1), lineBreaks);
	let character = characterArray(maxAmount);

	let input = character[rand.nextInt(character.length)];
	character = character.filter((e) => e != input);

	for (let i = 0; i < maxAmount - 1; i++) {
		if (i == read) {
			cases.push(
				normalCaseGen('"' + input + '"', logs.slice(i, i + 1), lineBreaks)
			);
		} else {
			let nextCase = character[rand.nextInt(character.length)];
			character = character.filter((e) => e != nextCase);
			cases.push(
				normalCaseGen('"' + nextCase + '"', logs.slice(i, i + 1), lineBreaks)
			);
		}
	}

	result = normalSwitchGen('"' + input + '"', cases, defaultCase);

	return result;
};

export const genLambda = (read, maxAmount, rand, lineBreaks) => {
	let result = "";

	let logs = logArray(maxAmount, groupSize);
	let cases = [];
	let defaultCase = lambdaDefaultGen(logs[maxAmount - 1], lineBreaks);

	let character = characterArray(maxAmount);

	let input = character[rand.nextInt(character.length)];
	character = character.filter((e) => e != input);

	for (let i = 0; i < maxAmount - 1; i++) {
		if (i == read) {
			cases.push(lambdaCaseGen('"' + input + '"', logs[i], lineBreaks));
		} else {
			let nextCase = character[rand.nextInt(character.length)];
			character = character.filter((e) => e != nextCase);
			cases.push(lambdaCaseGen('"' + nextCase + '"', logs[i], lineBreaks));
		}
	}

	result = lambdaSwitchGen('"' + input + '"', cases, defaultCase);

	return result;
};

export const genIf = (read, maxAmount, rand) => {
	let result = "";

	let logs = logArray(maxAmount, groupSize);

	let character = characterArray(maxAmount);

	let input = character[rand.nextInt(character.length)];
	character = character.filter((e) => e != input);

	let IF = "";
	if (read == 0) {
		IF = ifGen('"' + input + '"', '"' + input + '"', logs[0]);
	} else {
		let nextCase = character[rand.nextInt(character.length)];
		character = character.filter((e) => e != nextCase);

		IF = ifGen('"' + input + '"', '"' + nextCase + '"', logs[0]);
	}

	let ELSEIF = [];
	let ELSE = elseGen(logs[maxAmount - 1]);

	for (let i = 1; i < maxAmount - 1; i++) {
		if (i == read) {
			ELSEIF.push(elseIfGen('"' + input + '"', '"' + input + '"', logs[i]));
		} else {
			let nextCase = character[rand.nextInt(character.length)];
			character = character.filter((e) => e != nextCase);
			ELSEIF.push(elseIfGen('"' + input + '"', '"' + nextCase + '"', logs[i]));
		}
	}

	result = IF + ELSEIF.join("") + ELSE;

	return result;
};

export default generateSwitchOutput;
