const examples = {
	referenceSyntax: {
		switch:
			'//Switch\nswitch(variable){\n\tcase 1:\n\t\tlog("case 1");\n\t\tbreak;\n\t...\n\tdefault:\n\t\tlog("default");\n\t\tbreak;\n}',
		if: '//If\nif(variable == 1){\n\tlog("case 1");\n}\nelse if(variable == 2){\n\tlog("case 2");\n}\nelse{\n\tlog("default");\n}',
		lambdaSwitch:
			'//Lambda Switch\nSWITCH(variable)\n\t.CASE(1,\n\t\t() => log("case 1");\n\t)\n\t...\n\t.DEFAULT(\n\t\t() => log("default");\n\t);',
		for: "//For\nfor(let i = 0; i < list.size(); i++){\n\tlog(list[i]);\n}",
		forIterator:
			"//For + Iterator\nfor(let element : list){\n\tlog(element);\n}",
		forEach: "//ForEach\nlist.forEach(\n\te => log(e)\n);",
		repeat: "//Repeat\nrepeat(list,\n\te => log(e)\n);",
	},
	tasks: {
		"switch-ident": {
			question: "Gibt es ein lambda-switch Statement ?",
			snippet:
				'list.add(498)\n\nSWITCH(Element)\n\t.CASE(901,\n\t\t() => log("718")\n\t)\n\t.CASE(901,\n\t\t() => log("718")\n\t)\n\t.DEFAULT(\n\t\t() => log("default")\n\t);\n\nawait input("Please input a number: ");\n\nswitch(c){\n\tcase 718:\n\t\tlog("430");\n\t\tbreak;\n\tdefault:\n\t\tlog("default");\n\t\tbreak;\n}',
			answer: "1",
			possibleAnswers: "0 oder 1 (0 = Nein, 1 = Ja)",
		},
		"switch-output": {
			question: "Was ist die Ausgabe ?",
			snippet:
				'SWITCH("E")\n\t.CASE("G", \n\t\t() => log("1"))\n\t.CASE("F", \n\t\t() => log("1"))\n\t.CASE("K", \n\t\t() => log("1"))\n\t.CASE("H", \n\t\t() => log("1"))\n\t.CASE("J", \n\t\t() => log("2"))\n\t.CASE("A", \n\t\t() => log("2"))\n\t.CASE("L", \n\t\t() => log("2"))\n\t.CASE("E", \n\t\t() => log("2"))\n\t.CASE("B", \n\t\t() => log("3"))\n\t.CASE("I", \n\t\t() => log("3"))\n\t.CASE("C", \n\t\t() => log("3"))\n\t.DEFAULT(\n\t\t() => log("3"));',
			answer: "2",
			possibleAnswers: "1 - 3",
		},
		"loops-ident": {
			question: "Gibt es ein forEach Statement ?",
			snippet:
				"for(let c : list){ \n\tlog(c); \n }\n\nrepeat(collection,\n\t(c) => log(c)\n);)\n\nlist.add(462)\n\nlist = list.map(e => e != 441);",
			answer: "0",
			possibleAnswers: "0 oder 1 (0 = Nein, 1 = Ja)",
		},
		"loops-count": {
			question: "Wie viele repeat Schleifen gibt es ?",
			snippet:
				'for(let i = 0; i < 33; i++){\n\trepeat(collection,\n\t\t(Element) => log(Element)\n\t);)\n}\n\nrepeat(collection,\n\t(c) => log(c)\n);)\n\nSWITCH(Element)\n\t.CASE(873,\n\t() => {\n\t\trepeat(list,\n\t\t\t(Element) => log(Element)\n\t\t);)})\n\t.DEFAULT(\n\t\t() => log("default")\n\t);\n\nlist.add(353)\n\nlist = list.map(e => e != 1140);\n\nfor(let i = 0; i < list; i++){ \n\tlog(list[i]); \n}',
			answer: "3",
			possibleAnswers: "1 - 3",
		},
	},
};

export default examples;
