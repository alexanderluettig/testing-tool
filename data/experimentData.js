const experimentNames = [
	"Switch-Identification",
	"Switch-Output",
	"Loop-Count",
	"Loop-Identification",
	"Examples",
];
const experimentQuestions = {
	"Switch-Identification": "Gibt es ein TYPE Statement ?",
	"Switch-Output": "Was ist die Ausgabe ?",
	"Loop-Count": "Wie viele TYPE Statements gibt es ?",
	"Loop-Identification": "Gibt es ein TYPE Statement ?",
	Examples: "Eine Beispiel Frage mit TYPE",
};

export { experimentNames, experimentQuestions };
